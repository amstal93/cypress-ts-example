const visit = cy.visit.bind(cy)
const contains = cy.contains.bind(cy)
const screenshot = cy.screenshot.bind(cy)

describe('header TypeScript test', () => {
  it('is has expected text', () => {
    visit('index.html')
    contains('h2', 'test')
    screenshot('page')
  })
})
